﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace FSAdaptor
{
    [XmlRoot("UserDb")]
    public class UserDb: List<UserRecord>
    {
        public void WriteToFile(string filename)
        {
            TextWriter w = null;
            try
            {
                XmlSerializer s = new XmlSerializer(typeof(UserDb));
                w = new StreamWriter(filename);
                s.Serialize(w, this);
                w.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (w != null)
                {
                    w.Close();
                    w = null;
                }
            }
        }

        public static UserDb ReadFromFile(string filename)
        {
            UserDb newUserDb = null;
            TextReader r = null;
            try
            {
                XmlSerializer s = new XmlSerializer(typeof(UserDb));
                r = new StreamReader(filename);
                newUserDb = (UserDb)s.Deserialize(r);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                r.Close();
            }

            return newUserDb;
        }

        public UserRecord Lookup(int id)
        {
            foreach (UserRecord userRec in this)
            {
                if (userRec.ID == id)
                {
                    return userRec;
                }
            }

            return null;
        }
    }
}
