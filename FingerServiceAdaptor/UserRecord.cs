﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace FSAdaptor
{
    public class UserRecord
    {
        [XmlAttribute("id")]
        public int _internalScannerId;
        [XmlAttribute("name")]
        public string _carestoreId;

        public UserRecord()
        {
            _internalScannerId = 0;
            _carestoreId = string.Empty;
        }

        public UserRecord(int id, string name)
        {
            _internalScannerId = id;
            _carestoreId = name;
        }

        public int ID
        {
            get
            {
                return _internalScannerId;
            }
        }

        public string CareStoreId
        {
            get
            {
                return _carestoreId;
            }
        }
    }
}
