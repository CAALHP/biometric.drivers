﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Neurotec.Biometrics;

namespace FSAdaptor.Structs
{
    internal class EnrollmentResult
    {
        private NffvStatus _engineStatus;
        private NffvUser _engineUser;

        public NffvStatus EngineStatus
        {
            get { return _engineStatus; }
            set { _engineStatus = value; }
        }
    
        public NffvUser EngineUser
        {
            get { return _engineUser; }
            set { _engineUser = value; }
        }
    }
}
