﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Neurotec.Biometrics;

namespace FSAdaptor.Structs
{
    internal class VerificationResult
    {
        public void Clear()
        {
            Score = 0;
            EngineUser = null;
            EngineStatus = NffvStatus.None;
        }

        public NffvStatus EngineStatus { get; set; }
        public int Score { get; set; }
        public NffvUser EngineUser { get; set; }
    }
}
