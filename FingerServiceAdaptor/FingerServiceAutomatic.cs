﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;
using System.Xml.Linq;
using FSAdaptor.Events;
using FSAdaptor.Structs;
using Neurotec.Biometrics;

namespace FSAdaptor
{
    public class FingerServiceAutomatic
    {
        #region fields

        private readonly Nffv _engine;
        private bool _canEnroll = false;
        private string _newUserId;
        private readonly uint _enrollTimeOutDuration;
        private bool _isThreadRunning;
        private UserDb _userDb;
        private readonly string _userDataBaseFileName;
        private object _lock = new object();

        #endregion
        
        #region constructor

        public FingerServiceAutomatic()
        {
            string enginedbname;
            string scanner;

            try
            {
                XDocument doc = XDocument.Load(AppDomain.CurrentDomain.BaseDirectory + "Config.xml");

                var enrolltimeout = doc.Root.Element("enrolltimeout").Value;
                uint.TryParse(enrolltimeout, out _enrollTimeOutDuration);

                scanner = doc.Root.Element("scanner").Value;
                enginedbname = doc.Root.Element("enginedatabase").Value;
                _userDataBaseFileName = doc.Root.Element("userdatabase").Value;
            }
            catch (Exception e)
            {
                _enrollTimeOutDuration = 20000;
                scanner = "futronic";
                enginedbname = "DefaultEngine.dat";
                _userDataBaseFileName = "DefaultUser.xml";
            }


            try
            {
                _engine = new Nffv(enginedbname, "", scanner);
            }
            catch (Exception)
            {

                throw;
            }
            try
            {
                _userDb = UserDb.ReadFromFile(_userDataBaseFileName);
            }
            catch (Exception)
            {

                _userDb = new UserDb();
            }

            if (_engine.Users.Count > 0)
                Startthread();
        }

        #endregion
        
        #region public


        public void Dispose()
        {
            _isThreadRunning = false;
        }

        public void EnrollNewUser(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                RaiseEnrollEvent(Resource.FingerService_EnrollNewUser_Empty_username, false);
            }

            _newUserId = id;

            _canEnroll = true;

            if (!_isThreadRunning)
                Startthread();
        }

        public bool RemoveUser(string id)
        {
            bool result = false;
            if (_engine.Users.Count > 0)
            {
                lock (_lock)
                {
                    try
                    {
                        UserRecord userToBeRemoved = _userDb.First(x => x.CareStoreId == id);

                        _userDb.Remove(userToBeRemoved);
                        _engine.Users.RemoveAt(userToBeRemoved.ID);

                        result = true;
                    }
                    catch (Exception)
                    {
                        _userDb = UserDb.ReadFromFile(_userDataBaseFileName);
                        result = false;
                    }
                    finally
                    {
                        _userDb.WriteToFile(_userDataBaseFileName);
                    }
                }

            }

            return result;
        }


        public bool ClearDb()
        {
            bool result = false;

            if (_userDb.Count == 0) return result;

            lock (_lock)
            {
                try
                {
                    _userDb.Clear();
                    _engine.Users.Clear();

                    result = true;
                }
                catch (Exception)
                {

                    _userDb = UserDb.ReadFromFile(_userDataBaseFileName);
                    result = false;
                }
                finally
                {
                    _userDb.WriteToFile(_userDataBaseFileName);
                }
                return result;
            }
        }

        #endregion

        #region events

        public delegate void EnrollmentEventHandler(object sender, FingerEventArgs e);
        public delegate void VerifyEventHandler(object sender, VerifyEventArgs e);
        
        public EnrollmentEventHandler EnrollEvent;
        public VerifyEventHandler VerifiedEvent;

        protected void RaiseEnrollEvent(string msg, bool status)
        {
            var handler = EnrollEvent;
            if (handler != null) handler(this, new FingerEventArgs(msg, status));
        }

        protected void RaiseVerifiedEvent(string username)
        {
            var handler = VerifiedEvent;
            if (handler != null) handler(this, new VerifyEventArgs(username));

        }

        #endregion
        
        #region private

        private void Startthread()
        {
            if (!_isThreadRunning)
            {
                _isThreadRunning = true;
                Thread thread = new Thread(Running);
                thread.Start();
            }

        }



        private void Enroll()
        {
            NffvStatus enrollmentStatus;

            NffvUser enrollmentUser = _engine.Enroll(_enrollTimeOutDuration, out enrollmentStatus);

            if (enrollmentStatus == NffvStatus.TemplateCreated)
            {
                lock (_lock)
                {
                    _userDb.Add(new UserRecord(enrollmentUser.Id, _newUserId));
                }
                
                try
                {
                    lock (_lock)
                    {
                         _userDb.WriteToFile(_userDataBaseFileName);
                         RaiseEnrollEvent(_newUserId, true);
                    }
                }
                catch
                {
                    lock (_lock)
                    {
                        _engine.Users.RemoveAt(enrollmentUser.Id);
                    }
                    RaiseEnrollEvent(Resource.FingerService_Enroll_Failed_to_store_to_internal_database, false);
                }
                finally
                {
                    lock (_lock)
                    {
                        _userDb = UserDb.ReadFromFile(_userDataBaseFileName);
                    }
                }
            }
            else
            {
                RaiseEnrollEvent(enrollmentStatus.ToString(), false);
            }

            _canEnroll = false;
        }

        private void Running()
        {
            while (_isThreadRunning)
            {
                NffvUser verifyedUser = null;
                NffvStatus verificationStatus = NffvStatus.None;
                int score = 0;

                
                if (_engine.Users.Count > 0)
                {
                    while ((verificationStatus == NffvStatus.ScannerTimeout || verificationStatus == NffvStatus.None) && _isThreadRunning )
                    {
                        NffvUser usertoVerify;
                        lock (_lock)
                        {
                            if(_engine.Users.Count == 0) break;
                            usertoVerify = _engine.Users[0];
                            score = _engine.Verify(usertoVerify, 2000, out verificationStatus); 
                        }
                        if (score > 0)
                            verifyedUser = usertoVerify;
                    }

                    if (score == 0 && verificationStatus == NffvStatus.TemplateCreated)
                    {
                        for (int i = 1; i < _engine.Users.Count; i++)
                        {
                            NffvUser usertoVerify;
                            lock (_lock)
                            {
                                if (_engine.Users.Count == 0) break;
                                usertoVerify = _engine.Users[i];
                                score = _engine.Verify(usertoVerify, 2000, out verificationStatus);
                            }

                            if (score <= 0) continue;
                            verifyedUser = usertoVerify;
                            break;
                        }
                    }
                } 
                
                

                if (_canEnroll && score == 0)
                {
                    Enroll();
                    Sleep();
                }
                else if (score > 0 && _canEnroll)
                {
                    RaiseEnrollEvent(Resource.FingerService_Running_User_is_already_registreted, false);
                    _canEnroll = false;
                    Sleep();
                }
                else if (score > 0)
                { 
                    lock (_lock)
                    {
                        foreach (UserRecord record in _userDb.Where(record => verifyedUser.Id == record.ID))
                        {
                            RaiseVerifiedEvent(record.CareStoreId);
                            break;
                        }
                    }

                    Sleep();
                }
            }
        }

        private static void Sleep()
        {
            Thread.Sleep(5000);
        }

        #endregion

        
    }
}
