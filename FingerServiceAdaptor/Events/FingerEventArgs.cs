﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FSAdaptor.Events
{
    public class FingerEventArgs: EventArgs
    {
        private string _message;
        private bool _status;

        public FingerEventArgs(string message, bool status)
        {
            _message = message;
            _status = status;
        }

        public string Message
        {
            get { return _message; }
        }

        public bool Status
        {
            get { return _status; }
        }
    }
}
