﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FSAdaptor.Events
{
    public class VerifyEventArgs
    {
        private string _username;

        public VerifyEventArgs(string uname)
        {
            _username = uname;
        }

        public string Username
        {
            get { return _username; }
        }
    }
}
