﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CAALHP.Contracts;
using CAALHP.Events.Types;
using CAALHP.Events.UserServiceEvents;
using CAALHP.Utils.Helpers;
using CAALHP.Utils.Helpers.Serialization;
using CAALHP.Utils.Helpers.Serialization.JSON;
using FI.Events;
using FaceIdentificationAdaptor;

namespace SOA.ICE.FaceIdentificationDriver
{
    public class FaceIdentificationImplementation : IDeviceDriverCAALHPContract
    {
        private int _processId;
        public IDeviceDriverHostCAALHPContract Host { get; set; }
        private FaceIdentificationFacade _face;
        private bool _driverEnabled;

        public FaceIdentificationImplementation()
        {
            //Debugger.Launch();
            try
            {
                _face = FaceIdentificationFacade.Instance;
                // _face.OnBroadCastImage += OnBroadCastImage;
                _driverEnabled = true;
            }
            catch (Exception e)
            {
                _driverEnabled = false;
                Console.WriteLine("Error during initialization of FaceIdentification with message : {0} \n\n", e.Message);
            }

        }

        //private void OnBroadCastImage(object sender, NewImageArgs newImageArgs)
        //{
        //    var tmpimg = newImageArgs.Image();

        //    var imgAsString = CreateBase64Image(tmpimg)

        //    var imgEvent = new NewWebcamImageEvent
        //        {
        //            CallerName = GetName(),
        //            CallerProcessId = _processId,
        //            Image = newImageArgs.Image()
        //        };

        //    ReportEvent(imgEvent);
        //}

        //private string CreateBase64Image(byte[] fileBytes)
        //{
        //    Bitmap streamImage;

        //    using (MemoryStream ms = new MemoryStream(fileBytes))
        //    {
        //        streamImage = ScaleImage(Image.FromStream(ms));
        //    }
        //    using (MemoryStream ms = new MemoryStream())
        //    {
        //        streamImage.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
        //        return Convert.ToBase64String(ms.ToArray());
        //    }
        //}

        ///// <summary>
        ///// Resize the image to the specified width and height.
        ///// </summary>
        ///// <param name="image">The image to resize.</param>
        ///// <param name="width">The width to resize to.</param>
        ///// <param name="height">The height to resize to.</param>
        ///// <returns>The resized image.</returns>
        //public static System.Drawing.Bitmap ResizeImage(System.Drawing.Image image, int width, int height)
        //{
        //    //a holder for the result
        //    Bitmap result = new Bitmap(width, height);
        //    //set the resolutions the same to avoid cropping due to resolution differences
        //    result.SetResolution(image.HorizontalResolution, image.VerticalResolution);

        //    //use a graphics object to draw the resized image into the bitmap
        //    using (Graphics graphics = Graphics.FromImage(result))
        //    {
        //        //set the resize quality modes to high quality
        //        graphics.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
        //        graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
        //        graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
        //        //draw the image into the target bitmap
        //        graphics.DrawImage(image, 0, 0, result.Width, result.Height);
        //    }

        //    //return the resulting bitmap
        //    return result;
        //}

        public void Notify(KeyValuePair<string, string> notification)
        {
            Console.WriteLine("Notify called");
            var type = EventHelper.GetTypeFromFullyQualifiedNameSpace(notification.Key);
            dynamic obj = JsonSerializer.DeserializeEvent(notification.Value, type);
            HandleEvent(obj);
        }

        private void HandleEvent(BiometricsRequestEvent o)
        {
            if (!_driverEnabled) return;

            var resEvent = new BiometricsResponsEvent()
                {
                    CallerName = GetName(),
                    CallerProcessId = _processId,
                    Type = BiometricDeviceType.Facial
                };

            var serializedList = EventHelper.CreateEvent(SerializationType.Json, resEvent);
            Host.Host.ReportEvent(serializedList);
        }

        private void HandleEvent(DeleteUserRequestEvent o)
        {
            _face.RemoveEntry(o.UserId);
        }

        private void HandleEvent(FacialTemplateResponsEvent o)
        {
            //TODO There might be a issue with threadding here, if identifying is running at the same time this happens.
            if (o.Template == null || o.UserId == null) return;

            var template = o.Template;
            var userId = o.UserId;

            _face.AddEntry(userId, template);
        }

        private void HandleEvent(UserUpdateCompleteEvent o)
        {
            var updatedUserList = o.UserList;
            var updatedUserIdList = (from a in updatedUserList select a.UserId).ToList();

            var localList = _face.GetAllUserId();

            //foreach (var user in localList)
            //{
            //    string userId = user;
            //    var exsisting = updatedUserList.Single(x => x.UserId == userId);

            //    if (exsisting != null)
            //        updatedUserList.Remove(exsisting);
            //}

            updatedUserIdList.RemoveAll(localList.Contains);

            foreach (var userId in updatedUserIdList)
            {
                var templateReq = new FacialTemplateRequestEvent()
                    {
                        CallerName = GetName(),
                        CallerProcessId = _processId,
                        UserId = userId
                    };

                ReportEvent(templateReq);
            }
        }


        private void HandleEvent(RemoveUserRequestEvent o)
        {
            var userId = o.UserId;
            Event removeEvent;

            var result = _face.RemoveEntry(userId);

            if (result)
                removeEvent = new RemoveUserResponsEvent()
                {
                    CallerName = GetName(),
                    CallerProcessId = _processId,
                    UserId = userId
                };
            else
                removeEvent = new EnrollUserErrorResponsEvent()
                {
                    CallerName = GetName(),
                    CallerProcessId = _processId,
                    UserId = userId
                };

            ReportEvent(removeEvent);
        }

        private void HandleEvent(IdentificationRequestEvent o)
        {
            Console.WriteLine("Starting identification by requestevent");

            Event idEvent;
            try
            {
                var userId = _face.Identify();

                if (string.IsNullOrEmpty(userId))
                    idEvent = new IdentificationFailedResponsEvent()
                    {
                        CallerName = GetName(),
                        CallerProcessId = _processId
                    };
                else
                    idEvent = new IdentificationResponsEvent()
                    {
                        CallerName = GetName(),
                        CallerProcessId = _processId,
                        UserId = userId
                    };

            }
            catch (Exception ex)
            {
                idEvent = new IdentificationErrorResponsEvent()
                    {
                        CallerName = GetName(),
                        CallerProcessId = _processId,
                        Error = ex.Message
                    };
            }

            ReportEvent(idEvent);
        }

        private void HandleEvent(EnrollUserRequestEvent o)
        {
            var userId = o.UserId;
            Event enrollEvent;

            try
            {
                var result = _face.Identify();
                if (result != null)
                {
                    enrollEvent = new EnrollUserErrorResponsEvent()
                    {
                        CallerName = GetName(),
                        CallerProcessId = _processId,
                        UserId = userId,
                        Error = "User already exsist"
                    }; 
                }
                else
                {
                    var template = _face.EnrollNewPerson();

                    if (template != null)
                        _face.AddEntry(userId, template);

                    enrollEvent = new EnrollUserResponsEvent()
                    {
                        CallerName = GetName(),
                        CallerProcessId = _processId,
                        UserId = userId,
                        Template = template
                    };
                }
                //_face.EnrollNewPerson(userId);

            }
            catch (Exception e)
            {
                enrollEvent = new EnrollUserErrorResponsEvent()
                {
                    CallerName = GetName(),
                    CallerProcessId = _processId,
                    UserId = userId,
                    Error = e.Message
                };
            }

            ReportEvent(enrollEvent);
        }

        public void ReportEvent(Event e)
        {
            var serializedList = EventHelper.CreateEvent(SerializationType.Json, e, "FI.Events");
            Host.Host.ReportEvent(serializedList);
        }

        public string GetName()
        {
            return "FaceIdentificationDriver";
        }

        public bool IsAlive()
        {
            return true;
        }

        public void ShutDown()
        {
            Environment.Exit(0);
        }

        public void Initialize(IDeviceDriverHostCAALHPContract hostObj, int processId)
        {

            Console.WriteLine("FaceIdentificationDriver init started");
            Host = hostObj;
            _processId = processId;

            Console.WriteLine(hostObj.Host.ToString() + "\n " + processId);

            Host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(DeleteUserRequestEvent), "FI.Events"), _processId);
            Host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(FacialTemplateResponsEvent), "FI.Events"), _processId);
            Host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(RemoveUserRequestEvent), "FI.Events"), _processId);
            Host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(RemoveUserRequestEvent), "FI.Events"), _processId);
            Host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(IdentificationRequestEvent), "FI.Events"), _processId);
            Host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(EnrollUserRequestEvent), "FI.Events"), _processId);
            Host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(BiometricsRequestEvent)), _processId);
            Host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(UserUpdateCompleteEvent)), _processId);
            //  Host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(TareRequestEvent), "CW.Events"), _processId);

            Console.WriteLine("FaceIdentificationDriver Initialized");
        }
    }
}

