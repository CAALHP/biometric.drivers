﻿using System;
using System.Runtime.Serialization;
using Neurotec.IO;

namespace FaceIdentificationAdaptor
{
    [Serializable()]
    internal class Record : ISerializable
    {
        #region Private fields

        private string _id;
        private Neurotec.IO.NBuffer _template;

        #endregion

        #region Internal constructor

        internal Record(string id, Neurotec.IO.NBuffer template)
        {
            if (id == null) throw new ArgumentNullException("id");
            if (template == null) throw new ArgumentNullException("template");
            _id = id;
            _template = template;
        }

        #endregion

        #region Public properties

        public string Id
        {
            get
            {
                return _id;
            }
            set { _id = value; }
        }

        public Neurotec.IO.NBuffer Template
        {
            get
            {
                return _template;
            }
            set { _template = value; }
        }

        #endregion

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            byte[] templateAsByteArray = _template.ToArray();

            info.AddValue("id", _id, typeof(string));
            info.AddValue("template", templateAsByteArray, typeof(byte[]));
        }

            // The special constructor is used to deserialize values. 
        public Record(SerializationInfo info, StreamingContext context)
        {
            // Reset the property value using the GetValue method.
            _id = (string)info.GetValue("id", typeof(string));
            var templateAsByteArray = (byte[])info.GetValue("template", typeof(byte[]));
            _template = new NBuffer(templateAsByteArray);
        }
    }
}
