﻿using System;
using System.Drawing;

namespace FaceIdentificationAdaptor
{
    public class NewImageArgs : EventArgs
    {
        private Bitmap _image;
        public NewImageArgs(Bitmap img)
        {
            _image = img;
        }

        public Bitmap Image()
        {
            return _image;
        }
    }
}